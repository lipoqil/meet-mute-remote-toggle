# Meet mute remote toggle

## Getting started

1. Open _Meet mute remote toggle.scpt_ in Script Editor (should be default for that file)
2. Click _Run the Script_ in toolbar (button with play icon)

## Requirements

- **macOS** to run the included Apple Script file
- **[Flic button](https://flic.io)** as a remote (you can probably use any smart button)
  - This requires [Flic app](https://flic.io/mac-app) installed
- **Ruby** installed (my script is using 2.7.1, but you can change that)

## How does it work

Apple Script runs infinite loop with following steps

1. Start Ruby server waiting for request
2. Person pushes Flic button
3. Flic app makes request to Ruby server
4. Request ends the Ruby server
5. Apple Scripts presses the keystroke for toggling mute state
6. GOTO 1

## Flic app configuration

![Flic app configuration](Flic_app_configuration.png)
