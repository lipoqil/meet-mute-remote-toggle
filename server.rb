require 'webrick'

include WEBrick

server = WEBrick::HTTPServer.new :Port => 8000, :DocumentRoot => Dir.pwd
server.mount_proc '/kmn' do |req, res|
  res.body = 'Bye, world 😒'
  server.shutdown
end
server.start
